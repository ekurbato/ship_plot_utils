import ROOT as r
import numpy as np

def muon_shield (lgeofile, snd=False):
    geofile = r.TFile(lgeofile, 'r')
    geo = geofile.Get("FAIRGeom")
    cave_node = geo.GetTopNode()
    y = []
    x = []
    z = []
    gap_x = []
    gap_y = []

    snd_x = []
    snd_y = []
    snd_z = []
    
    for node in cave_node.GetNodes():
        for subnode in node.GetNodes():
            nodeName = subnode.GetName()
            if "MagRetR" in nodeName and 'Absorb' not in nodeName: 
                subnode.Print()
                zShift = subnode.GetMatrix().GetTranslation()[2]
                lVol =  subnode.GetVolume().GetShape()
                dots = lVol.GetVertices()
                dZ = lVol.GetDZ()
                vetz = [dots[i] for i in range(16)]
                Y = vetz[1::2]
                abs_Y = np.absolute(Y)
                abs_Y_f = np.sort(abs_Y[:4])
                abs_Y_l = np.sort(abs_Y[4:])
                
                z = z + [-dZ+zShift, -dZ+zShift, dZ+zShift, dZ+zShift, -dZ+zShift, None]
                y = y + [-max(Y[:4]), max(Y[:4]), max(Y[4:]), -max(Y[4:]), -max(Y[:4]), None]
                gap_y = gap_y + [-min(abs_Y_f), min(abs_Y_f), min(abs_Y_l), -min(abs_Y_l), -min(abs_Y_f), None]

            if "MagTopL" in nodeName and 'Absorb' not in nodeName: 
                subnode.Print()
                lVol =  subnode.GetVolume().GetShape()
                dots = lVol.GetVertices()
                vetz = [dots[i] for i in range(16)]
                X = vetz[::2]
                abs_X = np.absolute(X)
                abs_X_f = np.sort(abs_X[:4])
                abs_X_l = np.sort(abs_X[4:])
                
                gap_x = gap_x + [min(abs_X_f[1:3]), max(abs_X_f[1:3]), max(abs_X_l[1:3]), min(abs_X_l[1:3]), min(abs_X_f[1:3]), None]
                x = x + [-max(abs_X[:4]), max(abs_X[:4]), max(abs_X[4:]), -max(abs_X[4:]), -max(abs_X[:4]), None]

            if snd:
                if "TauNu" in nodeName: 
                    subnode.Print()
                    zShift = subnode.GetMatrix().GetTranslation()[2]
                    lVol =  subnode.GetVolume().GetShape()
                    dX = lVol.GetDX()
                    dY = lVol.GetDY()
                    dZ = lVol.GetDZ()
                    snd_z = snd_z + [-dZ+zShift, -dZ+zShift, dZ+zShift, dZ+zShift, -dZ+zShift, None]
                    snd_y = snd_y + [-dY, dY, dY, -dY, -dY, None]
                    snd_x = snd_x + [-dX, dX, dX, -dX, -dX, None]

    if snd:
        return x, y, z, gap_x, gap_y, snd_x, snd_y, snd_z
    else:
        return x, y, z, gap_x, gap_y
