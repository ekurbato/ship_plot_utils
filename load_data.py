from copy import deepcopy as copy
import uproot4 as uproot
import pandas as pd
import numpy as np
import awkward1 as ak
from pathlib import Path
from os.path import join, exists, isfile, isdir, basename
import os, fnmatch
from multiprocessing import Pool

def load_data_mp(mOpts):
    dirs, branches, treeName, lazy, lLibrary, lCut = mOpts
    dataBase = {'fPdgCode':uproot.AsJagged(uproot.AsDtype('>i4')),
             'fX':uproot.AsJagged(uproot.AsDtype('>f4')),
             'fY':uproot.AsJagged(uproot.AsDtype('>f4')),
             'fZ':uproot.AsJagged(uproot.AsDtype('>f4')),
             'fPx':uproot.AsJagged(uproot.AsDtype('>f4')),
             'fPy':uproot.AsJagged(uproot.AsDtype('>f4')),
             'fPz':uproot.AsJagged(uproot.AsDtype('>f4')),
             'fW':uproot.AsJagged(uproot.AsDtype('>f4')),
             'fStartX':uproot.AsJagged(uproot.AsDtype('>f4')),
             'fStartY':uproot.AsJagged(uproot.AsDtype('>f4')),
             'fStartZ':uproot.AsJagged(uproot.AsDtype('>f4')),
             'fTrackID':uproot.AsJagged(uproot.AsDtype('>i4')),
             'fEventId':uproot.AsJagged(uproot.AsDtype('>i4')),
             'fDetectorID':uproot.AsJagged(uproot.AsDtype('>i4')),
              'fLength':uproot.AsJagged(uproot.AsDtype('>f4'))
             }
    
    if lazy:
        print("lazy is not implemented")
        return 0
    outputs = {}
    outputs_keys = []
    for folder in dirs:
        lFile = folder
        if isfile(lFile):
            key = os.path.dirname(folder)
            nRepeats = 1
            while key in outputs_keys:
                nRepeats = nRepeats + 1
                key = key+"_"+str(nRepeats)
            outputs_keys.append(key)
            try:
                lTree = uproot.open(lFile)
                if any([treeName in lName for lName in lTree.keys()]):
                    lTree = lTree[treeName]
                else:
                    print("Bad file {}".format(lFile))
                    print(lTree.keys())
                    continue
                outputs[key] = {}
                branchdict = {}
                for branch in branches.keys():
                    if branch=='MCEventHeader.':
                        outputs[key][branch] = lTree[branch].arrays(['MCEventHeader.{}'.format(item) for item in branches[branch]], cut=lCut[branch] if lCut else None, library=lLibrary, how=dict)
                    else:
                        outputs[key][branch] = lTree[branch].arrays({'{}.{}'.format(branch,item):dataBase[item] for item in branches[branch]}, cut=lCut[branch] if lCut else None, library=lLibrary, how=dict)
                       
            except OSError:
                print("bad file {}".format(lFile))
                continue
        else:
            print ("no such file ",  join(folder, filename))
    return outputs

def process_uproot_mp(lConfig, lHists, fullSpill=False, pseudoBatch=False, drop_data=False, keepMC=False, single_file=False, keep_p=False, nCpu=1):
    exclude = []
    lhists = copy(lHists)
    outHists = copy(lHists)
    folder = lConfig['dir']
    fileName = lConfig['fileName']
    basePath = sorted(Path(folder).glob(f'**/{fileName}'))
    print("{} files to read in {}".format(len(basePath), folder))
    branches_to_load = outHists
    branches_to_load = {i:[k for k in ['fTrackID', 'fPdgCode', 'fX', 'fY', 'fZ', 'fDetectorID', 'fPx', 'fPy', 'fPz']] for i in outHists}
    branches_to_load['MCTrack'] = ['fPx', 'fPy', 'fPz', 'fStartX', 'fStartY', 'fStartZ', 'fPdgCode', 'fW']
    branches_to_load['MCEventHeader.'] = ['fEventId']
    myCuts = None
    if pseudoBatch:
        basePath = np.array_split(basePath, int(len(basePath)/pseudoBatch))
    else:
        basePath=[basePath]
    
    if single_file:
        basePath = [[os.path.join(lConfig['dir'], lConfig['fileName'])]]

    lConfig['barData'] = {}
    lConfig['err_y'] = {}
    lConfig["df_data"] = {}

    for bName in lhists:
        lConfig["df_data"][bName] = []
        if  drop_data:
            lConfig['barData'][bName] = []
            lConfig['err_y'][bName] = []
    if 'strawtubesPoint' in lhists:
            straw_index = lhists.index('strawtubesPoint')
    if keepMC:
        lConfig["df_data"]['MCTrack'] = []
    batches_in_total= len(basePath)
    opt_array = [[dirs, branches_to_load, 'cbmsim', False, "ak", None] for dirs in basePath]
    with Pool(processes=nCpu) as p:
        base = {}
        for new_element in p.imap_unordered(load_data_mp, opt_array):
            base.update(new_element)
        for key, data in base.items():
            lPath = key.split('/')
            if single_file:
                evKey = 0
            else:
                if fullSpill:
                    evKey = (int(lPath[-1])+1)*100000000 + (int(lPath[-2])+1)*10000000000
                else:
                    evKey = (int(lPath[-1])+1)*100000000

            data['MCEventHeader.']['eventID'] = data['MCEventHeader.']['MCEventHeader.fEventId']+evKey
        
        process_branches = outHists
        if keepMC:
            process_branches.append("MCTrack")
        tKey = next(iter(base.keys()))
        root_data = {lkey:{rkey:ak.concatenate([base[i][lkey][rkey] for i in base.keys()], axis=0) for rkey in base[tKey][lkey].keys()} for lkey in base[tKey].keys()}
        for key in process_branches:
            root_data[key]['event_id'] = ak.broadcast_arrays(root_data['MCEventHeader.']['eventID'], ak.zeros_like(root_data[key]['{}.fPdgCode'.format(key)]))[0]
            
            if 'MCTrack' not in key:
                root_data[key]['W'] = root_data['MCTrack']['MCTrack.fW'][root_data[key]['{}.fTrackID'.format(key)]]
        mu_only_data = {lKey:{key:root_data[lKey][key][abs(root_data[lKey]['{}.fPdgCode'.format(lKey)])==13] for key in root_data[lKey].keys()} for lKey in process_branches}
        
        for branch, data in mu_only_data.items():
            if branch not in lhists and branch != "MCTrack":
                continue
            flat = {key:ak.flatten(data[key], axis=None) for key in data.keys()}
            dfFlat = pd.DataFrame(flat)
            if branch=='strawtubesPoint':
                strawFlat = dfFlat.copy()
                for i in range(4):
                    bName = f"T{4-i}"
                    if bName not in lhists:
                        lhists.insert(straw_index, bName)
                        lConfig["df_data"][bName] = []
                        lConfig['barData'][bName] = []
                        lConfig['err_y'][bName] = []
                    bFlat = strawFlat[strawFlat['strawtubesPoint.fDetectorID']>=(4-i)*1e7]
                    strawFlat = strawFlat[strawFlat['strawtubesPoint.fDetectorID']<(4-i)*1e7]
                    reduced = bFlat.groupby(['event_id', '{}.fTrackID'.format(branch)], as_index=False).mean()
                    reduced['branch'] = bName
                    reduced.rename(columns={col:col.split('.')[-1] for col in reduced.columns}, inplace=True)
                    reduced['P'] = np.sqrt(np.square(reduced['fPx'])+np.square(reduced['fPy'])+np.square(reduced['fPz']))
                    reduced['Pt'] = np.sqrt(np.square(reduced['fPx'])+np.square(reduced['fPy'])) 
                    if keep_p:
                        reduced.drop(['fDetectorID'],axis=1, inplace=True)
                    else:
                        reduced.drop(['fDetectorID', 'fPx', 'fPy', 'fPz'],axis=1, inplace=True)
                    lConfig["df_data"][bName].append(reduced)
            if branch != 'MCTrack':
                reduced = dfFlat.groupby(['event_id', '{}.fTrackID'.format(branch)], as_index=False).mean()
            else:
                reduced = dfFlat
            reduced['branch'] = branch
            reduced.rename(columns={col:col.split('.')[-1] for col in reduced.columns}, inplace=True)
            reduced['P'] = np.sqrt(np.square(reduced['fPx'])+np.square(reduced['fPy'])+np.square(reduced['fPz']))
            reduced['Pt'] = np.sqrt(np.square(reduced['fPx'])+np.square(reduced['fPy']))
            if branch != "MCTrack":
                if keep_p:
                        reduced.drop(['fDetectorID'],axis=1, inplace=True)
                else:
                        reduced.drop(['fDetectorID', 'fPx', 'fPy', 'fPz'],axis=1, inplace=True)
            else:
                if not keep_p:
                    reduced.drop(['fPx', 'fPy', 'fPz'],axis=1, inplace=True)
            if not drop_data:
                lConfig["df_data"][branch].append(reduced)
            else:
                lConfig['barData'][branch].append(reduced['W'].sum())
                lConfig['err_y'][branch].append(np.sqrt(np.sum(np.square(reduced['W']))))

    if 'strawtubesPoint' in lhists:
        lhists.remove('strawtubesPoint')
    
    for branch in lConfig["df_data"].keys():
        print(branch)
        if branch not in lhists and branch != "MCTrack":
            continue
        if not drop_data:
            lConfig["df_data"][branch] = pd.concat(lConfig["df_data"][branch], ignore_index=True)
            if branch != "MCTrack":
                lConfig['barData'][branch] = lConfig["df_data"][branch]['W'].sum()
                lConfig['err_y'][branch] = np.sqrt(np.sum(np.square(lConfig["df_data"][branch]['W'])))
        else:
            if branch != "MCTrack":
                lConfig['barData'][branch] = np.sum(lConfig['barData'][branch])
                lConfig['err_y'][branch] = np.sqrt(np.sum(np.square(lConfig['err_y'][branch])))
    return lhists
