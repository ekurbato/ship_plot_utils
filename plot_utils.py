import numpy as np
import pandas as pd
import plotly.graph_objects as go
import plotly.express as px
import matplotlib.pylab as plt
from matplotlib.colors import Normalize
from plotly.subplots import make_subplots
from mpl_toolkits.axes_grid1 import ImageGrid
from itertools import chain
import matplotlib
import json

def make_station_plot(lconfig, station, lrange=((-300,300),(0,350)), forceFmax=None, what=['fX', 'P'], lbins=(50,50), with_wheights=True, weights_col="W", query=None, add_v_lines=None):
    lPlotData = {}
    metrics = {}
    metrics['fX'] = "cm"
    metrics['fY'] = "cm"
    metrics['P'] = "GeV"
    metrics['Pt'] = "GeV"
    
    for i, item in enumerate(lconfig.keys()):
        if query:
            lPlotData[item] = np.histogram2d(lconfig[item]['df_data'][station].query(query)[what[0]], lconfig[item]['df_data'][station].query(query)[what[1]], bins=lbins, 
                                  weights=lconfig[item]['df_data'][station].query(query)[weights_col] if with_wheights else None,
                                  range=lrange)
        else:
            lPlotData[item] = np.histogram2d(lconfig[item]['df_data'][station][what[0]], lconfig[item]['df_data'][station][what[1]], bins=lbins, 
                                  weights=lconfig[item]['df_data'][station][weights_col] if with_wheights else None,
                                  range=lrange)
    vmax = max([np.amax(lPlotData[key][0]) for key in lPlotData.keys()])
    print(vmax)
    if forceFmax:
        vmax=forceFmax
    fig, axs = plt.subplots(1, len(lconfig), figsize=(15,5))
    if len(lconfig)==1:
        axs=[axs]
    for i, key in enumerate(lconfig.keys()):
        im = axs[i].imshow(lPlotData[key][0].transpose()[::-1], norm=matplotlib.colors.LogNorm(vmin=0.01, vmax=vmax), 
                           extent=[lPlotData[key][1][0],lPlotData[key][1][-1],lPlotData[key][2][0],lPlotData[key][2][-1]], cmap=plt.cm.plasma)
        axs[i].set_title(key)
        axs[i].set_xlabel(f"{what[0]}, {metrics[what[0]]}")
        axs[i].set_ylabel(f"{what[1]}, {metrics[what[1]]}")
        if add_v_lines:
            axs[i].vlines((-add_v_lines, add_v_lines), *lrange[1])
    fig.subplots_adjust(right=0.8)
    fig.patch.set_facecolor('white')
    fig.patch.set_alpha(1)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    fig.colorbar(im, cax=cbar_ax)
    fig.suptitle(f"{station} station")
    plt.show()
def reverse_fcn(fval, wval):
    W_star = 1915820.
    return fval/(1. + np.exp(10. * (wval - W_star) / W_star)) - 1.
    
    
    
def load_db(in_path):
    with open(in_path) as db_f:
        db = json.load(db_f)
        db = pd.DataFrame(db).sort_values('iteration')
        db['iteration'] =  db['iteration'].astype(int)
        db.index = db['iteration']
        fcn_keys = db['fcns'].iloc[0].keys()
        new_cols = pd.DataFrame(db['fcns'].to_list(), index=db['iteration'])
        db = db.join(pd.DataFrame(db['fcns'].to_list(), index=db['iteration']))
        for key in fcn_keys:
            if key=='nRuns':
                continue
            db['pure_'+str(key)] = reverse_fcn(db[key], db['W'])
        db.drop('fcns', axis=1,inplace=True)
        print(f"We have {db['iteration'].max()} points in database")
        return db, list(fcn_keys)


def plot_db(db, fcns_t, only_flux=False, exlude=None):
    fig = go.Figure()
    for fcn in fcns_t:
        if exlude and  fcn in exlude or fcn=='nRuns':
#         if fcn=='nRuns':
            continue
        fig.add_trace(go.Scatter(x=db['iteration'], 
                                 y=db["pure_"+str(fcn)] if only_flux else db[fcn],
                                mode='lines',
                                name=fcn))
        fig.update_xaxes(rangemode="tozero")
        fig.update_yaxes(rangemode="tozero", type='log')

    fig.show()
    
def plot_step_db(db, fcns_t, only_flux=False, exlude=None):
    fig = go.Figure()
    for fcn in fcns_t:
        if exlude and  fcn in exlude or fcn=='nRuns':
#         if fcn=='nRuns':
            continue
        fig.add_trace(go.Scatter(x=db['iteration'], 
                                 y=np.minimum.accumulate(db["pure_"+str(fcn)] if only_flux else db[fcn]),
                                mode='lines',
                                name=fcn))
        fig.update_xaxes(rangemode="tozero")
        fig.update_yaxes(rangemode="tozero", type='log')

    fig.show()
    
def print_params_from_iteration(data, iteration):
    result = data.query(f'iteration=={iteration}')['parameters'].item()
    print(result)
    return(result)
    
def plot_arrays_from_params(paramsShield, is_sc=False, return_fig=False):
    dZ = paramsShield[0:8]
    dX_in = paramsShield[8::6]
    dX_out = paramsShield[9::6]
    dY_in = paramsShield[10::6]
    dY_out = paramsShield[11::6]
    gap_in = paramsShield[12::6]
    gap_out = paramsShield[13::6]

    yGaps = []
    xGaps=[]
    
    plot_z = []
    plot_x = []
    plot_y = []
    
    for i in range(2, 8):
        yGaps = yGaps + [-dY_in[i], dY_in[i], dY_out[i], -dY_out[i], -dY_in[i], None]
        xGaps = xGaps + [dX_in[i], dX_in[i]+gap_in[i], dX_out[i]+gap_out[i] , dX_out[i], dX_in[i], None]
        c_z = 0 if len(plot_z) == 0 else plot_z[-3]
        plot_z = plot_z + [c_z+10, c_z+10, c_z+10 + 2*dZ[i], c_z + 10 + 2*dZ[i], c_z + 10, None]
        if is_sc and i == 3:
            plot_x = plot_x + [-(4*dX_in[i]+gap_in[i]), (4*dX_in[i]+gap_in[i]), (4*dX_out[i]+gap_out[i]), -(4*dX_out[i]+gap_out[i]), -(4*dX_in[i]+gap_in[i]), None]
            plot_y = plot_y + [-(3*dX_in[i]+dY_in[i]), (3*dX_in[i]+dY_in[i]), (3*dX_out[i]+dY_out[i]), -(3*dX_out[i]+dY_out[i]), -(3*dX_in[i]+dY_in[i]), None]
        else:
            plot_x = plot_x + [-(2*dX_in[i]+gap_in[i]), (2*dX_in[i]+gap_in[i]), (2*dX_out[i]+gap_out[i]), -(2*dX_out[i]+gap_out[i]), -(2*dX_in[i]+gap_in[i]), None]
            plot_y = plot_y + [-(dX_in[i]+dY_in[i]), (dX_in[i]+dY_in[i]), (dX_out[i]+dY_out[i]), -(dX_out[i]+dY_out[i]), -(dX_in[i]+dY_in[i]), None]
    return([plot_x, plot_y, plot_z, xGaps, yGaps])
def plot_shield_from_verts(plot_verts, what_to_plot=0, return_fig=False):
    gap_color = 'rgba(168, 235, 203, 0.5)'
    up_color = 'rgba(255,99,71, 0.8)'
    down_color = 'rgba(135,206,250, 0.8)'
    x, y, z, gap_x, gap_y = plot_verts
    
    zMax = np.max([ik for ik in z if ik is not None])
    zMin = np.min([ik for ik in z if ik is not None])
    half_position = np.argwhere(np.invert([np.isscalar(xi) for xi in x])).flatten()[2]
#     print(half_position)
#     print(half_position)
    if what_to_plot==0:
        fig = go.Figure(data=[
            go.Scatter(x=z[:half_position], y=x[:half_position], fill="toself", fillcolor=up_color, line_color=up_color, marker=dict(opacity=0), meta=dict(name="Up polarity")),
            go.Scatter(x=z[half_position:], y=x[half_position:], fill="toself", fillcolor=down_color, line_color=down_color, marker=dict(opacity=0), meta=dict(name="Up polarity")), 
            go.Scatter(x=z, y=gap_x, fill="toself", fillcolor=gap_color, line_color=gap_color, marker=dict(opacity=0)),
            go.Scatter(x=z, y=[-p if p else None for p in gap_x], fill="toself", fillcolor=gap_color, line_color=gap_color, marker=dict(opacity=0))
               ],
               layout=go.Layout(
                xaxis=dict(range=[zMin, zMax+50], autorange=False),
                yaxis=dict(range=[-330, 330], autorange=False)))
    else:
        fig = go.Figure(data=[
            go.Scatter(x=z[:half_position], y=y[:half_position], fill="toself", fillcolor=up_color, line_color=up_color, marker=dict(opacity=0), meta=dict(name="Up polarity")),
            go.Scatter(x=z[half_position:], y=y[half_position:], fill="toself", fillcolor=down_color, line_color=down_color, marker=dict(opacity=0), meta=dict(name="Up polarity")), 
            go.Scatter(x=z, y=gap_y, fill="toself", fillcolor=gap_color, line_color=gap_color, marker=dict(opacity=0))
               ],
               layout=go.Layout(
                xaxis=dict(range=[zMin, zMax+50], autorange=False),
                yaxis=dict(range=[-330, 330], autorange=False)))
                
    if return_fig:
        return fig
    else:
        fig.show()
